//Kho

(function() {

  var map, infoWindow;
  var directionsDisplay;
  var directionsService = new google.maps.DirectionsService();

  window.onload = function() {

    // Creating a map
    var options = {  
      zoom: 17,  
      center: new google.maps.LatLng(55.396119, 10.387159),  
      mapTypeId: google.maps.MapTypeId.ROADMAP 
      
    };  
    
    
    var styles = [
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "weight": 0.1 },
      { "color": "#042ffd" }
    ]
  },{
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#55fd64" }
    ]
  }
];    
    

    var map = new google.maps.Map(document.getElementById('map'), options);  
  map.setOptions({styles: styles});  

  }
})();
