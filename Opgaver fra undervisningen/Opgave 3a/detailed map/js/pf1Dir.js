//Kho

(function() {

  var map, infoWindow;
  var directionsDisplay;
  var directionsService = new google.maps.DirectionsService();

  window.onload = function() {

    // Creating a map
    var options = {  
      zoom: 17,  
      center: new google.maps.LatLng(55.396119, 10.387159),  
      mapTypeId: google.maps.MapTypeId.ROADMAP 
      
    };  
    
    
    var styles = [
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "weight": 0.1 },
      { "color": "#042ffd" }
    ]
  },{
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#55fd64" }
    ]
  }
];    
    
  directionsDisplay = new google.maps.DirectionsRenderer();

    var map = new google.maps.Map(document.getElementById('map'), options);  
  map.setOptions({styles: styles});  
  directionsDisplay.setMap(map);
    
    // Defining different MarkerImages for different states
    var bicycleShop = new google.maps.MarkerImage(
      'img/odenseCity/normal/bicycle_shop.png', 
      new google.maps.Size(32, 37),
      new google.maps.Point(0, 0), 
      new google.maps.Point(16, 35)
    );

    var bicycleShopHover = new google.maps.MarkerImage(
      'img/odenseCity/hover/bicycle_shop.png', 
      new google.maps.Size(32, 37),
      new google.maps.Point(0, 0), 
      new google.maps.Point(16, 35)
    );

    var bicycleShopClick = new google.maps.MarkerImage(
      'img/odenseCity/click/bicycle_shop.png', 
      new google.maps.Size(32, 37),
      new google.maps.Point(0, 0), 
      new google.maps.Point(16, 35)
    );    
    
    
    var travelAgency = new google.maps.MarkerImage(
      'img/odenseCity/normal/travel_agency.png', 
      new google.maps.Size(32, 37),
      new google.maps.Point(0, 0), 
      new google.maps.Point(16, 35)
    );    
    
    
    
    // Creating the shadow
    var shadow = new google.maps.MarkerImage(
      'img/shadow.png',
      null, 
      null,
      new google.maps.Point(16, 35)
    );
    

  var bicycleShopPosition = new google.maps.LatLng(55.396339,10.384504);
    // Creating the marker
    var marker = new google.maps.Marker({
     position: bicycleShopPosition, 
      map: map,
      icon: bicycleShop,
      shadow: shadow
    });
    
    // Creating the marker
   var travelAgencyPosition = new google.maps.LatLng(55.396300,10.388621);
    var markerTravelAgency = new google.maps.Marker({
     position: travelAgencyPosition, 
      map: map,
      icon: travelAgency,
      shadow: shadow
    });    
    
    
function calcRoute() {
  var start = bicycleShopPosition;
  var end = travelAgencyPosition;
  console.log(start + "  " + end);

  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.DirectionsTravelMode.WALKING
  };
  

  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
  

}    
    
    
    // Adding events that will alter the look of the marker
    
   
    // Hover
    google.maps.event.addListener(marker, 'mouseover', function() {
      this.setIcon(bicycleShopHover);
    });

    google.maps.event.addListener(marker, 'mouseout', function() {
      this.setIcon(bicycleShop);
    });
 
    // Click
    google.maps.event.addListener(marker, 'mousedown', function() {
      this.setIcon(bicycleShopClick);
    });
    
    google.maps.event.addListener(marker, 'mouseup', function() {
      this.setIcon(bicycleShopHover);
    });    
    
        google.maps.event.addListener(marker, 'click', function() {
    
      // Creating the div that will contain the detail map
      var detailDiv = document.createElement('div');
      detailDiv.style.width = '200px';
      detailDiv.style.height = '200px';
      document.getElementById('map').appendChild(detailDiv);
      
      // Creating MapOptions for the overview map
      var overviewOpts = {
        zoom: 16,
        center: marker.getPosition(),
        mapTypeId: map.getMapTypeId(),
        disableDefaultUI: true
      };
      
      var detailMap = new google.maps.Map(detailDiv, overviewOpts);
      
      // Create a marker that will show in the detail map
      var detailMarker = new google.maps.Marker({
        position: marker.getPosition(),
        map: detailMap,
        icon: bicycleShop,
        shadow: shadow,
        clickable: false
      });
      
      // Check to see if an InfoWindow already exists
      if (!infoWindow) {
        infoWindow = new google.maps.InfoWindow();
      }
      
      // Setting the content of the InfoWindow to the detail map
      infoWindow.setContent(detailDiv);
      
      // Opening the InfoWindow
      infoWindow.open(map, marker);
    
    });
calcRoute();
  }
})();
