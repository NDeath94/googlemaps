//Kho

(function() {

  var map, infoWindow;
  var directionsDisplay;
  var directionsService = new google.maps.DirectionsService();

  window.onload = function() {

    // Creating a map
    var options = {  
      zoom: 17,  
      center: new google.maps.LatLng(55.396119, 10.387159),  
			mapTypeControlOptions: {mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'OSM']}
  
    };  
    
    
    var styles = [
  {
    "featureType": "water",
    "elementType": "geometry.fill",
    "stylers": [
      { "visibility": "on" },
      { "weight": 0.1 },
      { "color": "#042ffd" }
    ]
  },{
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      { "visibility": "on" },
      { "color": "#55fd64" }
    ]
  }
];    
    

    var map = new google.maps.Map(document.getElementById('map'), options);  
    map.setOptions({styles: styles});  
    
					
					//Defining OSM Map Type
					var osmMapType = new google.maps.ImageMapType({
   			         	getTileUrl: function(coord, zoom) {
                    		return "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
                		},
                		tileSize: new google.maps.Size(256, 256),
                		name: "OpenStreetMap",
                		maxZoom: 18
            		});
            		
            		//relate new mapTypeId to the ImageMapType - osmMapType object
					map.mapTypes.set('OSM', osmMapType);
					
					//set this new mapTypeId to be displayed
					map.setMapTypeId('OSM');    

  }
})();
