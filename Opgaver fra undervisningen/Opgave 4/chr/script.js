
var markerPath = [];

function initMap() {

    //Create new Google Map
    var mapCenter = {lat: 56, lng: 10};
    var mapOptions = {
        center: mapCenter,
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);


    //Attach click event handler to the map.
    google.maps.event.addListener(map, 'click', function (e) {
        placeMarker(map, e);
        drawPolyline(map, e);
    });

}

function placeMarker(map, e){

    //Determine the location where the mouse has clicked.
    var location = e.latLng;
    map.panTo(location); //Move center of map to latest placed marker.

    markerPath.push(location);

    //Create a marker and place it on the map.
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    console.log(markerPath);
}


function drawPolyline(map, e){
    console.log("Draw polyline");



    var markerPath = new google.maps.Polyline({
        path: markerPath,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 5
    });
    markerPath.setMap(map);
}